package com.telusko.Ajaxdemo;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EmpController {

	Logger logger = LoggerFactory.getLogger(EmpController.class);
	@Autowired
	IEmpService empservice;

	@RequestMapping("/")
	public String home() {
		logger.info("Calling to webpage");
		return "Test";
	}

	
	@RequestMapping(value = "/Testfiles")
    public @ResponseBody Map<String, String> upload(@RequestParam("EName") String EName,
			@RequestParam("ECity") String ECity) {
		Map<String, String> resultmap = new HashMap<>();
		logger.info("Calling to EmpService");
		EmpDemo result = empservice.data(EName, ECity);
		String Eid = String.valueOf(result.getId());
		System.out.println("Employee id is" + " " + Eid);
		resultmap.put("Employeeid", Eid);
		return resultmap;
	}

	@RequestMapping(value = "getEmp")
	public String getEmp(@RequestParam("ID") Integer ID, Model m) {

		// List<Alien> aliens=Arrays.asList(new Alien(1,"blue","shirish"));
//		m.addAttribute("Ro",aliens);
		// m.addAttribute(Ro,repo.getOne(eid));
		// m.addAttribute("ro",repo.getOne(eid));
		empservice.get(ID, m);
		return "Result";
	}
}
