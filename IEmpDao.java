package com.telusko.Ajaxdemo;

import org.springframework.ui.Model;

public interface IEmpDao {
	public EmpDemo data(String EName,String ECity);
	
	public String get(Integer ID,Model m);

}
