package com.telusko.Ajaxdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
@Repository
public class EmpDaoImpl  implements IEmpDao{
	@Autowired
	Emprepo repo;
	Logger logger= LoggerFactory.getLogger(EmpDaoImpl.class);
	public EmpDemo data(String EName,String ECity)
	{
logger.info("EmpDAO called successfully");
	
		
		try {			
			
			EmpDemo obj=new EmpDemo();
			obj.setECity(ECity);
			obj.setEName(EName);
			repo.save(obj);
			System.out.println("Row added successfully");
			return obj;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	return null;
	}
	public String get(Integer ID,Model m)
	{
logger.info("EmpDAO called successfully");
	
		
		try {
			
			
			
			m.addAttribute("output","repo.getOne(ID)");
			System.out.println("Row Found");
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "m";
	}
	

}
