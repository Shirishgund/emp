package com.telusko.Ajaxdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;


@Service
public class EmpService implements IEmpService {
	
	Logger logger= LoggerFactory.getLogger(EmpService.class);
	@Autowired
	IEmpDao empdao;
	public EmpDemo data(String EName,String ECity)
	{ 
		logger.info("Service is runnings now  and calling DAO");
		return empdao.data(EName, ECity);
	}
	public String get(Integer ID,Model m) {
		logger.info("Service running and calling DAO");
		return empdao.get(ID, m);
	}
	}


