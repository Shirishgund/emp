package com.telusko.Ajaxdemo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
@Entity
public class EmpDemo {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_Sequence")
	@SequenceGenerator(name = "id_Sequence", sequenceName = "ID_SEQ",allocationSize=1)

	private int id;
	private String EName;
	private String ECity;
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public String getEName() {
		return EName;
	}



	public void setEName(String eName) {
		EName = eName;
	}



	public String getECity() {
		return ECity;
	}



	@Override
	public String toString() {
		return "EmpDemo [id=" + id + ", EName=" + EName + ", ECity=" + ECity + "]";
	}



	public void setECity(String eCity) {
		ECity = eCity;
	}



	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

	}

}
